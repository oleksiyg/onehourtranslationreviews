jQuery(document).ready(function () {
        jQuery("#testimonial-demo").owlCarousel({
            navigation: true, // Show next and prev buttons
            autoPlay: 6000, //Set AutoPlay to 3 seconds
            autoplayHoverPause: true,
            paginationSpeed: 400,
            singleItem: true,
            pagination: false,
            navigationText: [
                "<i class='fa fa-angle-left'></i>",
                "<i class='fa fa-angle-right'></i>"
            ]
       });
        jQuery(document).ready(function () {

            jQuery("#blog-slide").owlCarousel({
                navigation: false, // Show next and prev buttons
                singleItem: true,
                autoplayHoverPause: false,
                pagination: false,
                autoPlay: 6000, //Set AutoPlay to 3 seconds
                loop: true
            });

        });
});
