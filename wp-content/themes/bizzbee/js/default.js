function headeranim() {
    if (jQuery(window).width() > 767) {
        jQuery("header").css({position: "fixed"});
        jQuery("header").next().css({"padding-top": jQuery("header").height()});
    }
    else {
        jQuery("header").css({"position": 'relative'});
        jQuery("header").next().css({"padding-top": '0'});
    }
}
jQuery(window).resize(function () {
    headeranim();
    if (jQuery(window).width() > 768) {
        $menu = jQuery("nav#main-menu").find("ul:first");
        $menu.css('display', 'block');
    }
    if (jQuery(window).width() < 767) {
        $menu = jQuery("nav#main-menu").find("ul:first");
        $menu.css('display', 'none');
    }
});
jQuery(document).ready(function ($) {
    headeranim();
    //Mobile Menu
    $("#dt-menu-toggle").on("click", function (event) {
        event.preventDefault();
        var $menu;
        $menu = $("nav#main-menu").find("ul:first");
        $menu.slideToggle(function () {
            $menu.css('overflow', 'visible');
            $menu.toggleClass('menu-toggle-open');
        });
    });

    //Mobile Menu End


});
