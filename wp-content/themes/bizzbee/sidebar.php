<?php 
/**
 * The right sidebar template file
**/
if(is_page_template('page-templates/left-sidebar.php')){ 
	$bizzbee_offset_class= '';
}else{
	$bizzbee_offset_class= 'col-md-offset-1';
}
?>
<div class="col-md-4 col-sm-12 <?php echo $bizzbee_offset_class; ?>">
	<div class="sidebar main-sidebar">
		<?php if ( is_active_sidebar( 'sidebar-1' ) ) { 
			 dynamic_sidebar( 'sidebar-1' );
		} ?>
	 </div>
</div>
