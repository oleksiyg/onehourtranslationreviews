<?php
/**
 * The main index template file
 * */
get_header();
?>
<section <?php post_class(); ?>>
	<div class="inner-pages">
		<div class="page-title col-md-12">
			<div class="container theme-container">
				<div class="row">
					<div class="col-md-6 col-sm-6 page-title-captions">
						<h4>
							<?php $blogtitle_check = get_theme_mod( 'bizzbee_blogtitle' );
								if( $blogtitle_check != '' ) {  
									echo esc_attr( get_theme_mod('bizzbee_blogtitle', '') );
								 } else { 	
									echo _e('Our Blog','bizzbee');
							 } ?>
						</h4>
					</div>
				</div>
			</div>
		</div>
	
		<?php get_template_part('content'); ?>
	</div>
</section>

<?php get_footer(); ?>