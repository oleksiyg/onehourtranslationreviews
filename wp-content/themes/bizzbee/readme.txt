=== BizzBee ===

BizzBee Theme by FruitThemes http://fruitthemes.com

=== About BizzBee ===

BizzBee is a lightweight, clean, super flexible and bootstrap based responsive theme for business or corprate, personal websites. BizzBee Theme is built on top of Twitter Bootstrap which means that your site can be displayed on every device (pc, laptop, mobile devices, tablets etc) without horizontal scrolling. It has got custom widgets, theme-options etc in order to get it customized as per your need.

=== Tags ===

left-sidebar, fixed-layout, fluid-layout, responsive-layout, custom-menu, editor-style, featured-images, full-width-template, post-formats, sticky-post, theme-options, threaded-comments, translation-ready


== License ==

License: GNU General Public License v3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

== Copyright ==
BizzBee Theme, Copyright 2015 fruitthemes.com
BizzBee is distributed under the terms of the GNU GPL

== Features ==

Responsive design(Bootstrap), Theme Options, Custom Menu, supports social media.

== Installation ==

1. Primary:
 = Login to your wp-admin account and go to Appearance -> Themes.
 = Select "Install" tab and click on the "Upload" link.
 = Select "bizzbee.zip" and click on "Install Now" button.
 = In case of errors, use an alternate method.

2. Alternate:
 = Unzip the template file (bizzbee.zip) that you have downloaded.
 = Upload the entire folder (bizzbee) to your server via FTP and place it in the /wp-content/themes/ folder.
 = Do not change the directory name.
 = The template files should be there now: /wp-content/themes/bizzbee/index.php (example).

3. Log into your WP admin panel and click on "Appearance". Go to "Themes" tab.

4. Now click on "BizzBee" to activate the theme.

5. Theme Customizer	Options:	

	a) Basic Settings:
           You can manage here site logo, favicon, blog title and contact information text.	

	b) Home Page Settings:
		i)    Slider Section	: You can add Soliloque Slider Id.
		ii)   Welcome To bizzbee : You can manage title, description, image.
		iii)  Our path to Perfection : You can set home page title, description and image.
        iv)   What You Get : You can manage title, description, right side image and left side text.
        v)    Blog Section : you can select category of posts and add title, description of posts slider.
			
---------------------------------------------------------
License and Copyrights for Resources used in this theme
---------------------------------------------------------

i) Fontawesome
==============
	License URI:  http://fontawesome.io/license/
	Resource Name: fontawesome
	License Name: Open Font License (OFL) Version 4.1.0

ii) Fonts
===========
	http://fonts.googleapis.com/css?family=Open+Sans
	This is under SIL Open Font.

iii)Bootstrap files
==================
	we used are licensed under the Apache License v3.3.1
	Bootstrap - http://getbootstrap.com
	Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)

iv) html5 js file
=================
	we used are licensed under MIT/GPL2.
	Version - v3.7.0
	
v) OwlCarousel
============
we used are licensed under MIT.
Version - v1.3.3


---------------------------------------------------------
Plugin Support
---------------------------------------------------------
I) Meta Slider support for the following plugins:
	- Responsive WordPress Slider - Meta Slider
	- Resource URI: https://wordpress.org/plugins/ml-slider/
	- Compatible up to: 4.1.5	

---------------------------------------------------------
TGM Plugin license and link
---------------------------------------------------------

 license -  http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 link    -  https://github.com/thomasgriffin/TGM-Plugin-Activation
	

---------------------------------------------------------
Image
---------------------------------------------------------
	
	Copyright: Pixabay
	Resource URI: http://pixabay.com/en/skyline-new-york-city-cityscape-731496/
	License: CC0 1.0
	License URI: http://creativecommons.org/publicdomain/zero/1.0/deed.en
