<?php
/*
 * Set up the content width value based on the theme's design.
 */
if ( ! function_exists( 'bizzbee_setup' ) ) :
function bizzbee_setup() {	
	
	register_nav_menus( array(
		'primary'   => __( 'Main Menu', 'bizzbee' ),	
	) );
	global $content_width;
	if ( ! isset( $content_width ) ) $content_width = 730;
	
	load_theme_textdomain( 'bizzbee', get_template_directory() . '/languages' );	
	add_theme_support('automatic-feed-links');
	add_theme_support('post-thumbnails');
	set_post_thumbnail_size(730, 404, true);
	add_image_size('bizzbee-post-image', 670, 380, true);
	add_image_size('bizzbee-home-tab-size', 380, 270, true);	
	add_image_size('bizzbee-custom-widget-size',100, 80, true);
	add_image_size('bizzbee-blog-thumbnail-image-home',570, 320, true);
	
	add_theme_support('html5', array(
	   'search-form', 'comment-form', 'comment-list',
	));
	add_theme_support( 'custom-header', apply_filters( 'bizzbee_custom_header_args', array(
	'uploads'       => true,
	'flex-height'   => true,
	'default-text-color' => '#000',
	'header-text' => true,
	'height' => '110',
	'width'  => '1300'
 	) ) );
	add_theme_support( 'custom-background', apply_filters( 'bizzbee_custom_background_args', array(
	'default-color' => 'f5f5f5',
	) ) );	
	     
	add_filter('use_default_gallery_style', '__return_false'); 
	add_editor_style('css/editor-style.css');
	add_theme_support( 'title-tag' );	  	
}

function bizzbee_change_excerpt_more( $more ) {
	global $post;
    return '<div class="theme-btn-group col-md-12"><a class="default-btn" title="'. __('Read More','bizzbee').'" href="'. esc_url(get_permalink($post->ID)) . '">' .  __('Read More','bizzbee'). '</a></div>';          
}
add_filter('excerpt_more', 'bizzbee_change_excerpt_more');



function bizzbee_excerpt_length( $length ) {
    return (!is_page_template('page-templates/front-page.php')) ? 60 : 30;
}
add_filter( 'excerpt_length', 'bizzbee_excerpt_length', 999 );

endif; // bizzbee_setup
add_action( 'after_setup_theme', 'bizzbee_setup' );

add_action('wp_head','bizzbee_purpose_bg_img_css');
function bizzbee_purpose_bg_img_css()
{
	$bizzbee_breadcrumbs_image_bg=get_theme_mod('bizzbee_breadcrumbs_image_bg');
	if (!empty($bizzbee_breadcrumbs_image_bg) ){
		$bizzbee_breadcrumbs_image_bg = esc_url(get_theme_mod('bizzbee_breadcrumbs_image_bg'));
		$bizzbee_breadcrumbs_output="<style> .background-section { background-image :url('".$bizzbee_breadcrumbs_image_bg."');
		background-position: center;} </style>";
		echo $bizzbee_breadcrumbs_output;
	}
	
	$bizzbee_breadcrumbs_image_bg=get_theme_mod('bizzbee_breadcrumbs_blog_image_bg');
	if (!empty($bizzbee_breadcrumbs_image_bg) ){
		$bizzbee_breadcrumbs_image_bg = esc_url(get_theme_mod('bizzbee_breadcrumbs_blog_image_bg'));
		$bizzbee_breadcrumbs_output="<style> .background-blog { background-image :url('".$bizzbee_breadcrumbs_image_bg."');
		background-position: center;} </style>";
		echo $bizzbee_breadcrumbs_output;
	}
	
	
}
add_action('wp_head','bizzbee_favicon');
function bizzbee_favicon(){
	if ( get_theme_mod( 'bizzbee_favicon' ) ) {
		?><link rel="shortcut icon" href="<?php echo esc_url(get_theme_mod( 'bizzbee_favicon' )); ?>"> <?php
	}
}
/*** Enqueue css and js files ***/
require get_template_directory() . '/functions/enqueue-files.php';

/*** Enqueue breadcrumbs files ***/
require get_template_directory() . '/functions/breadcrumbs.php';

/*** default setup files ***/
require get_template_directory() . '/functions/theme-default-setup.php';

/*** Customizer ***/
require get_template_directory() . '/functions/theme-customizer.php';

require get_template_directory() . '/functions/custom-header.php';

require get_template_directory() . '/functions/tgm-plugins.php';

/*** Pro feature***/
require get_template_directory() . '/theme-options/theme-option.php';

?>
