<footer class="main-footer col-md-12 col-sm-12  col-xs-12">
  <?php if (is_active_sidebar('footer-1') || is_active_sidebar('footer-2') || is_active_sidebar('footer-3') || is_active_sidebar('footer-4')) { ?>
      <div class="footer-top col-md-12 col-sm-12  col-xs-12 ">
			<div class="container">
              <div class="row">				      
          <?php if (is_active_sidebar('footer-1')) { ?>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <?php dynamic_sidebar('footer-1'); ?>
             </div>
          <?php } ?>
          <?php if (is_active_sidebar('footer-2')) { ?>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <?php dynamic_sidebar('footer-2'); ?>
            </div>
          <?php } ?>
          <?php if (is_active_sidebar('footer-3')) { ?>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <?php dynamic_sidebar('footer-3'); ?>
             </div>
          <?php } ?>	
          <?php if (is_active_sidebar('footer-4')) { ?>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <?php dynamic_sidebar('footer-4'); ?>
             </div>
          <?php } ?>	
        </div>      
      </div>
    </div>
  <?php } ?>
  
   <div class="footer-bottom col-md-12 col-sm-12  col-xs-12">
      <div class="container theme-container">
        <p>
 <?php
		 $bizzbee_copyright_check = get_theme_mod( 'copyright_url_setting' );
				if( $bizzbee_copyright_check != '' ) {
					 echo esc_html( get_theme_mod('copyright_url_setting', '') );   } 
			 printf( __( 'Powered by %1$s and %2$s ', 'bizzbee' ), '<a href="http://wordpress.org" target="_blank">WordPress</a>', '<a href="http://fruitthemes.com/wordpress-themes/bizzbee" target="_blank">BizzBee</a>' ); 
			
			 ?>
		</p>
      </div>
   </div>
</footer>
<?php wp_footer(); ?>        
</body>
</html>
