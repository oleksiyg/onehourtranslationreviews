<?php
/**
 * The Header template file
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">  
    <!--[if lt IE 9]>
	    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/html5.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>       
    <header class="inner-header">
	   <div class="container theme-container">
		  <div class="row">	
     
               <div class="col-md-2 col-sm-2 header-logo">   
				   <?php  if ( get_theme_mod( 'bizzbee_logo' ) ) { ?>
							<a href='<?php echo esc_url( home_url( '/' ) ); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'><img src='<?php echo esc_url( get_theme_mod( 'bizzbee_logo' ) ); ?>' alt='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' class="img-responsive"></a>
                        <?php } else { ?>
                            <a href="<?php echo esc_url(home_url('/')); ?>">
									<h3 class="site-title logo-box"><?php bloginfo( 'name' ); ?></h3>
									<h5 class="site-description"><?php bloginfo( 'description' ); ?></h5>
                            </a>
                        <?php }  ?>
				</div>  
                                            
                <div class="col-md-10 col-sm-10 header-menu">               
                  <nav id="main-menu">
					  <div id="dt-menu-toggle" class="dt-menu-toggle">
						  <?php _e('Menu','bizzbee'); ?>
						 <span class="dt-menu-toggle-icon"></span>
					  </div>  
                    <?php
                    if (has_nav_menu('primary')) {
                      $bizzbee_defaults = array(
                          'theme_location' => 'primary',
                          'container' => '',
                          'container_class' => 'collapse navbar-collapse nav_coll',
                          'container_id' => 'example-navbar-collapse',
                          'menu_class' => 'overline',
                          'menu_id' => '',
                          'submenu_class' => '',
                          'echo' => true,
                          'before' => '',
                          'after' => '',
                          'link_before' => '',
                          'link_after' => '',
                          'items_wrap' => '<ul id="%1$s menu" class="%2$s">%3$s</ul>',
                          'depth' => 0
                      );
                      wp_nav_menu($bizzbee_defaults);
                    }
                    ?>                                                
                  </nav>	             
                </div>
     
		  </div>	
      </div>	
   </header>
    <?php
    $bizzbee_header_image = get_header_image();
    if (!empty($bizzbee_header_image)) {
      ?>
      <div class="webpage-container container custom-header">
        <img src="<?php echo esc_url($bizzbee_header_image); ?>" class="header-image img-responsive" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="<?php _e('custom-header', 'bizzbee') ?>" />
      </div>
    <?php } ?>
