<?php
/*
 * The default template for displaying content
 */
?>
<div class="section-row col-md-12 col-sm-12 col-xs-12">
	<div class="container theme-container">
		<div class="row">
			<div class="content-blog col-md-7">
				<?php while (have_posts()) : the_post(); ?>
				<div class="ourblog-box">
					<?php if ( has_post_thumbnail() ) : ?>
					<div class="post-header">
						<div class="image-wrapper">
							<div class="blur-img"></div>
								<a href="<?php echo esc_url(get_permalink()); ?>"> 
								  <?php the_post_thumbnail( 'bizzbee-post-image', array( 'alt' => get_the_title(), 'class' => 'img-responsive') ); ?>
								</a>
						</div>
					</div>
				   <?php endif; ?>							
					<div class="post-detail">
						<a href="<?php echo esc_url(get_permalink()); ?>" class="post-title"><?php the_title(); ?></a>
 					    <?php bizzbee_entry_meta(); ?>
						<?php the_excerpt(); ?>	
					</div>
				</div>
				<?php endwhile; ?> 
				<div class="site-pagination col-md-12 col-sm-12">      
					<nav role="navigation" class="navigation pagination">
						<div class="nav-links">
							 <?php
								// Previous/next page navigation.
								the_posts_pagination();
							?>
						</div>
					</nav>		
				</div>
			</div>
			<?php get_sidebar(); ?>
			</div>
	</div>
</div>
	

