<?php
/**
 * 404 page template file
 * */
get_header();
?>
<section>
<div class="inner-pages">
		<div class="page-title col-md-12">
			<div class="container theme-container">
				<div class="row">
					<div class="col-md-6 col-sm-6 page-title-captions">
						<h4>
							<?php _e('404 - Article Not Found', 'bizzbee'); ?>
						</h4>
					</div>
					<div class="col-md-6 col-sm-6 breadcrumbs">
						<ul>
							<?php bizzbee_custom_breadcrumbs(); ?>
						</ul>
					</div>
				</div>
			</div>
		</div>

    <!-- 404 Content Start -->
    <div class="container theme-content">
        <div class="page-article">
            <div class="row blog-page">
                <div class="col-md-12 col-sm-12 no-padding">
                    <div class="jumbotron">
                        <h1><?php _e('Epic 404 - Article Not Found', 'bizzbee') ?></h1>
                        <p><?php _e("This is embarassing. We can't find what you were looking for.", "bizzbee") ?></p>
                        <p><?php _e('Whatever you were looking for was not found, but maybe try looking again or search using the form below.', 'bizzbee') ?></p>
                        <div class="row">
                            <div class="col-sm-12 search-formmain">
                                <?php get_search_form(); ?>
                            </div>
                        </div>
                    </div>            
                </div>
            </div> </div>
    </div>
    <!-- 404 Content End -->
</div>    
</section>
<?php get_footer(); ?>
