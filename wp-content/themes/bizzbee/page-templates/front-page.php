<?php
/*
 * Template Name: Home Page
 */
get_header();
?>
<!--Slider Start-->
<div class="home-banner col-md-12 col-sm-12 col-xs-12">
<?php  $bizzbee_metaslider = get_theme_mod( 'bizzbee_metaslider' );
		if(!empty($bizzbee_metaslider)){
			echo do_shortcode('[metaslider id="'.$bizzbee_metaslider.'"]');
		}
?>
</div>
<section>
	<!--Welcome section-->
	<?php
		$bizzbee_welcome_title = get_theme_mod( 'bizzbee_Welcome_title' ); 
		$bizzbee_Welcome_info = get_theme_mod( 'bizzbee_Welcome_info' );
		$bizzbee_Welcome_righttitle = get_theme_mod( 'bizzbee_Welcome_righttitle' );
		$bizzbee_Welcome_rightinfo = get_theme_mod( 'bizzbee_Welcome_rightinfo' );
		$bizzbee_Welcome_centerimg = get_theme_mod( 'bizzbee_Welcome_image_bg' );
		$bizzbee_Welcome_lefttitle = get_theme_mod( 'bizzbee_Welcome_lefttitle' );
		$bizzbee_Welcome_leftinfo = get_theme_mod( 'bizzbee_Welcome_leftinfo' );
	
		if(!empty($bizzbee_welcome_title) || 
			!empty($bizzbee_Welcome_info) || 
			!empty($bizzbee_Welcome_righttitle) || 
			!empty($bizzbee_Welcome_rightinfo) || 
			!empty($bizzbee_Welcome_centerimg) ||
			!empty($bizzbee_Welcome_lefttitle) || 
			!empty($bizzbee_Welcome_leftinfo)) { 
	?>
	
	<div class="section-row welcome-section col-md-12 col-sm-12  col-xs-12">
		<div class="container theme-container">
			<?php if(!empty($bizzbee_welcome_title) || !empty($bizzbee_Welcome_info) ) { ?>
			<div class="main-title col-md-8 col-md-offset-2">
				<?php if(!empty($bizzbee_welcome_title)) { ?>
					<h2><span><?php echo esc_attr( get_theme_mod('bizzbee_Welcome_title') );  ?></span></h2>
				<?php } if(!empty($bizzbee_Welcome_info)) { ?>
					<p><?php echo esc_attr(get_theme_mod('bizzbee_Welcome_info')); ?></p>
				<?php } ?>
			</div>
			<?php } 
			if(!empty($bizzbee_Welcome_righttitle) || 
				!empty($bizzbee_Welcome_rightinfo) || 
				!empty($bizzbee_Welcome_centerimg) ||
				!empty($bizzbee_Welcome_lefttitle) || 
				!empty($bizzbee_Welcome_leftinfo)) { 
			?>
			<div class="welcome-box col-md-12">
				<div class="row">
					<?php if(!empty($bizzbee_Welcome_righttitle) || !empty($bizzbee_Welcome_rightinfo)) { ?>
					<div class="col-md-3 col-sm-4 welcome-content">
						<?php if(!empty($bizzbee_Welcome_righttitle)) { ?>
							<h3><?php echo esc_attr( get_theme_mod('bizzbee_Welcome_righttitle') );  ?></h3>
						<?php } if(!empty($bizzbee_Welcome_rightinfo)) { ?>
							<p><?php echo esc_attr(get_theme_mod('bizzbee_Welcome_rightinfo')); ?></p>
						<?php } ?>
					</div>
					<?php } 
						if(!empty($bizzbee_Welcome_centerimg)) { ?>
					<div class="col-md-6 col-sm-4">
						<div class="welcome-img">
							<img src="<?php echo esc_url( get_theme_mod( 'bizzbee_Welcome_image_bg' ) ); ?>" alt="<?php _e('Welcome','bizzbee'); ?>">
						</div>
					</div>
					<?php }
					if(!empty($bizzbee_Welcome_lefttitle) || !empty($bizzbee_Welcome_leftinfo)) {
					?>
					<div class="col-md-3 col-sm-4 welcome-content">
						<?php if(!empty($bizzbee_Welcome_lefttitle)) { ?>
							<h3><?php echo esc_attr( get_theme_mod('bizzbee_Welcome_lefttitle') );  ?></h3>
						<?php } if(!empty($bizzbee_Welcome_leftinfo)) { ?>
							<p><?php echo esc_attr(get_theme_mod('bizzbee_Welcome_leftinfo')); ?></p>
						<?php } ?>
					</div>
					<?php } ?>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>            
	
	<?php } ?>
	<!--Welcome section end-->

	<!--Background section-->
	<?php $bizzbee_perfection_title = get_theme_mod( 'bizzbee_perfectiontitle' ); 
			$bizzbee_perfection_info = get_theme_mod( 'bizzbee_perfectioninfo' );
			$bizzbee_perfection_button = get_theme_mod( 'bizzbee_perfection_buttontitle' );
			$bizzbee_perfection_link = get_theme_mod( 'bizzbee_perfection_buttonlink' );
			?>
	<?php if(!empty($bizzbee_perfection_title) || !empty($bizzbee_perfection_info) || !empty($bizzbee_perfection_button) ) { ?>		
	<div class="section-row background-section col-md-12 col-sm-12  col-xs-12">
		<div class="blur-bg"></div>
		<div class="half-circle-top"></div>
		<div class="container theme-container">
			<?php if(!empty($bizzbee_perfection_title) || !empty($bizzbee_perfection_info) ) { ?>
				<div class="main-title full-title col-md-10 col-md-offset-1">
					<?php if(!empty($bizzbee_perfection_title)) { ?>
						<h2><span><?php echo esc_attr( get_theme_mod('bizzbee_perfectiontitle', '') );  ?></span></h2>
					<?php } 
					 if(!empty($bizzbee_perfection_info)) { ?>
						<p><?php echo esc_attr(get_theme_mod('bizzbee_perfectioninfo', '')); ?></p>
					<?php } ?>
				</div>
			<?php } 
				if(!empty($bizzbee_perfection_link) && !empty($bizzbee_perfection_button)) { ?>
			<div class="theme-btn-group col-md-12">
				<a href="<?php echo esc_attr( get_theme_mod('bizzbee_perfection_buttonlink', '') );  ?>" class="default-btn"><?php echo esc_attr( get_theme_mod('bizzbee_perfection_buttontitle', '') );  ?></a>
			</div>
			<?php } ?>
		</div>
		
	</div>
	<?php } ?>
	<!--background section end-->

	<!--what you get section-->
	<?php
		$bizzbee_you_get_title = get_theme_mod( 'bizzbee_get_title' );
		$bizzbee_you_get_info = get_theme_mod( 'bizzbee_get_info' ); 
		$bizzbee_you_get_right_img = get_theme_mod( 'bizzbee_youget_image_bg' );
		$bizzbee_you_get_left_text = get_theme_mod( 'bizzbee_get_left_text' ); 
		
		if(!empty($bizzbee_you_get_title) || 
			!empty($bizzbee_you_get_info) || 
			!empty($bizzbee_you_get_right_img) || 
			!empty($bizzbee_you_get_left_text)) {
	?>
	<div class="section-row col-md-12 col-sm-12  col-xs-12">
		<div class="container theme-container">
			<?php if(!empty($bizzbee_you_get_title) || 
						!empty($bizzbee_you_get_info) ) { ?>
				<div class="main-title col-md-8 col-md-offset-2">
					<?php if(!empty($bizzbee_you_get_title)) { ?>
						<h2><span><?php echo esc_attr( get_theme_mod('bizzbee_get_title', '') );  ?></span></h2>
					<?php } if(!empty($bizzbee_you_get_info)) { ?>
						<p><?php echo esc_attr( get_theme_mod('bizzbee_get_info', '') );  ?></p>
					<?php } ?>
				</div>
			<?php } 
				if(!empty($bizzbee_you_get_right_img) || 
					!empty($bizzbee_you_get_left_text)) {
			?>
			<div class="get-column col-md-12">
				<div class="row">
					<?php if(!empty($bizzbee_you_get_right_img)) { ?>
					<div class="col-md-6 col-sm-6 get-content">
						<img src="<?php echo esc_url( get_theme_mod( 'bizzbee_youget_image_bg' ) ); ?>" alt="What You Get">
					</div>
					<?php } if(!empty($bizzbee_you_get_left_text)) { ?>
					<div class="col-md-6 col-sm-6 get-content">
						<p><?php echo esc_attr( get_theme_mod('bizzbee_get_left_text', '') );  ?></p>
					</div>
					<?php } ?>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
	<?php } ?>
	<!--end what you get section-->

	<!--blog section-->
	<div class="section-row background-blog col-md-12 col-sm-12  col-xs-12">
		<div class="blur-bg"></div>
		<div class="half-circle-top"></div>
		<div class="container theme-container">
			<div class="main-title col-md-8 col-md-offset-2">
				<h2><span>
					<?php $blog_check = get_theme_mod( 'bizzbee_blog_title' );
					if(!empty($blog_check)) {
						 echo esc_attr( get_theme_mod('bizzbee_blog_title', '') ); 
					 } else { 
						echo _e('Our Blog', 'bizzbee');  
				 } ?>
					</span></h2>
			</div>
			<div class="our-blog col-md-12 col-sm-12">
				<div id="blog-slide" class="owl-carousel owl-theme">
					<?php
			$bizzbee_blogcategory=get_theme_mod('bizzbee_blogcategory');
			$bizzbee_args = array(
				'ignore_sticky_posts' => '1',
				'meta_query' => array(
					array(
						'key' => '_thumbnail_id',
						'compare' => 'EXISTS'
					),
				)
			);
			if(!empty($bizzbee_blogcategory))
				$bizzbee_args['cat']=$bizzbee_blogcategory;
				$bizzbee_query = new WP_Query($bizzbee_args);
				if ($bizzbee_query->have_posts()) : while ($bizzbee_query->have_posts()) : $bizzbee_query->the_post();
			?>
					<div class="item">
						<div class="col-md-12 col-sm-12">
							<div class="ourblog-box">
								<div class="col-md-6 col-sm-12 no-padding col-xs-12">
									<div class="post-header">
										<div class="image-wrapper">
											<div class="blur-img"></div>
												<a href="<?php echo esc_url( get_permalink() ); ?>">
													<?php the_post_thumbnail( 'bizzbee-blog-thumbnail-image-home', array( 'alt' => get_the_title(), 'class' => 'img-responsive') ); ?>
												</a>
										</div>
									</div>
								</div>
								<div class="col-md-6 col-sm-12 no-padding col-xs-12">
									<div class="post-detail">
										<a class="post-title" href="<?php echo esc_url(get_permalink()); ?>"><?php the_title(); ?></a>
										<div class="post-meta">
												<?php bizzbee_entry_meta(); ?>
										</div>
										<?php the_excerpt(); ?>  
									</div>
								</div>
							</div>
						</div>
					</div>
			<?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
		<?php endif; ?>  
							</div>
						</div>
					</div>
			
				</div>
			</div>
		</div>
	</div>
	<!--blog  section end-->

</section>
<!--section part end-->


<?php get_footer(); ?>
