<?php
/**
 * Template Name: Left sidebar 
 * */
get_header();
?>
<section <?php post_class(); ?>>
	<div class="inner-pages">
		<div class="page-title col-md-12">
			<div class="container theme-container">
				<div class="row">
					<div class="col-md-6 col-sm-6 page-title-captions">
						<h4><?php the_title(); ?></h4>
					</div>
					<div class="col-md-6 col-sm-6 breadcrumbs">
						<ul>
							<?php bizzbee_custom_breadcrumbs(); ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	
		<div class="section-row col-md-12 col-sm-12 col-xs-12">
			<div class="container theme-container">
				<div class="row">
					<?php get_sidebar(); ?>
					<div class="content-blog col-md-7 col-md-offset-1">
						<?php get_template_part( 'content', 'page' );  ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>
