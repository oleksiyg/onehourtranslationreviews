<?php
/**
 * The template used for displaying page content
 */
?>
<?php while (have_posts()) : the_post(); ?>
	<div class="ourblog-box">
		<?php if ( has_post_thumbnail() ) : ?>
		<div class="post-header">
			<div class="image-wrapper">
				<div class="blur-img"></div>
					<a href="<?php echo esc_url(get_permalink()); ?>"> 
					  <?php 
					  if(is_page_template('page-templates/full-width.php')){
							the_post_thumbnail( 'full', array( 'alt' => get_the_title(), 'class' => 'img-responsive') ); 
					  }else{
							the_post_thumbnail( 'bizzbee-post-image', array( 'alt' => get_the_title(), 'class' => 'img-responsive') ); 
					  }		
							?>
					</a>
			</div>
		</div>
	   <?php endif; ?>							
		<div class="post-detail">
			<a href="<?php echo esc_url(get_permalink()); ?>" class="post-title"><?php the_title(); ?></a>
			<?php the_content(); ?>	
		</div>
	</div>
<?php endwhile; ?> 