<?php
/**
 * The Archive template file
 * */
get_header();
?>
<section>
<div class="inner-pages">
		<div class="page-title col-md-12">
			<div class="container theme-container">
				<div class="row">
					<div class="col-md-6 col-sm-6 page-title-captions">
						<h4><?php the_archive_title(); ?></h4>
					</div>
					<div class="col-md-6 col-sm-6 breadcrumbs">
						<ul>
							<?php bizzbee_custom_breadcrumbs(); ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	<?php  get_template_part('content');?>
</div>		
</section>
<?php get_footer(); ?>
