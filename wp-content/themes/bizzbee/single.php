<?php
/**
 * The Single template file
 * */
get_header();
?>
<!--section part start-->
<section>
	<div class="inner-pages">
		<div class="page-title col-md-12">
			<div class="container theme-container">
				<div class="row">
					<div class="col-md-6 col-sm-6 page-title-captions">
						<h4><?php the_title(); ?></h4>
					</div>
					<div class="col-md-6 col-sm-6 breadcrumbs">
						<ul>
							<?php bizzbee_custom_breadcrumbs(); ?>
						</ul>
					</div>
				</div>
			</div>
		</div>

<div class="section-row col-md-12 col-sm-12 col-xs-12">
	<div class="container theme-container">
		<div class="row">
			<div class="content-blog col-md-7">
				<?php while (have_posts()) : the_post(); ?>	
				<div class="ourblog-box">
					<?php if ( has_post_thumbnail() ) : ?>
						<div class="post-header">
							<div class="image-wrapper">
								<div class="blur-img"></div>
									  <?php the_post_thumbnail( 'bizzbee-post-image', array( 'alt' => get_the_title(), 'class' => 'img-responsive') ); ?>
							</div>
						</div>
					   <?php endif; ?>	

					<div class="post-detail">
						<span href="#" class="post-title"><?php the_title(); ?></span>
						<div class="post-meta">
						   <?php bizzbee_entry_meta(); ?>    
						</div>
						<?php
							the_content();
							wp_link_pages(array(
								'before' => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'bizzbee') . '</span>',
								'after' => '</div>',
								'link_before' => '<span>',
								'link_after' => '</span>',
							));
						?>
					</div>
				</div>

				<div class="post-pagination col-md-12 no-padding">
					<div class="row">
						<?php
							the_post_navigation(array(
								'next_text' =>
								'<span class="page-numbers previous-post pre default-btn" > %title </span>',
								'prev_text' =>
								'<span class="page-numbers next-post nex default-btn"> %title </span>',
							));
						?>
					</div>
				</div>
				<?php endwhile; ?>
				<div class="comments-article">
				   <div class="clearfix"></div> 
				   <?php comments_template('', true); ?>
				</div>
		   </div>
		  <?php get_sidebar(); ?>
		</div>
	</div>
</div>
	</div>
</section>
<!--section part end-->


<?php get_footer(); ?> 
