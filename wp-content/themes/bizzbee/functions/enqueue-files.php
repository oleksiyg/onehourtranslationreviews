<?php
/*
 * bizzbee Enqueue css and js files
*/
function bizzbee_enqueue() {
  wp_enqueue_style('bizzbee-bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array(), '04302015', '');
  wp_enqueue_style('bizzbee-font-awesome', get_template_directory_uri() . '/css/font-awesome.css', array(), '04302015', '');
  wp_enqueue_style('bizzbee-theme', get_template_directory_uri() . '/css/theme.css', array());
  wp_enqueue_style('bizzbee-style', get_stylesheet_uri());

  wp_enqueue_script('bizzbee-bootstrapjs', get_template_directory_uri() . '/js/bootstrap.js', array('jquery'));
  wp_enqueue_script('bizzbee-defaultjs', get_template_directory_uri() . '/js/default.js', array('jquery'));
  

  if (is_page_template('page-templates/front-page.php')) {
	wp_enqueue_script('bizzbee-homepagejs', get_template_directory_uri() . '/js/homepage.js', array('jquery'));  
    wp_enqueue_style('bizzbee-owl-curousel', get_template_directory_uri() . '/css/owl.carousel.css', array(), '04302015', '');
    wp_enqueue_script('bizzbee-owl-curousel-js', get_template_directory_uri() . '/js/owl.carousel.js', array('jquery'));
  }
  if (is_singular())
    wp_enqueue_script("comment-reply");
}
add_action('wp_enqueue_scripts', 'bizzbee_enqueue');
