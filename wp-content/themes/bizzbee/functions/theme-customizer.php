<?php
function bizzbee_theme_customizer( $wp_customize ) {
    /* sections */
    $wp_customize->add_section( 'bizzbee_basic_section' , array(
    'title'       => __( 'Basic Settings', 'bizzbee' ),
    'priority'    => 30,
	) );
        
	
	$wp_customize->add_panel( 'home_id', array(
		'capability'     => 'edit_theme_options',
		'theme_supports' => '',
		'title'          => __('Home Page Settings','bizzbee'),
		'description'    => '',
		'priority'    => 30,
	) );
	$wp_customize->add_section( 'bizzbee_silder_section' , array(
		'title'       => __( 'Slider Section', 'bizzbee' ),
		'priority'    => 30,
		'panel'  => 'home_id',
	) );
	$wp_customize->add_section( 'bizzbee_welcome_section' , array(
		'title'       => __( 'Welcome To bizzbee', 'bizzbee' ),
		'priority'    => 30,
		'panel'  => 'home_id',
	) );
	$wp_customize->add_section( 'bizzbee_perfection_section' , array(
		'title'       => __( 'Our path to Perfection', 'bizzbee' ),
		'priority'    => 30,
		'panel'  => 'home_id',
	) );
	$wp_customize->add_section( 'bizzbee_whatyouget_section' , array(
		'title'       => __( 'What You Get', 'bizzbee' ),
		'priority'    => 30,
		'panel'  => 'home_id',
	) );

	$wp_customize->add_section( 'bizzbee_blog_section' , array(
		'title'       => __( 'Blog Section', 'bizzbee' ),
		'priority'    => 30,
		'panel'  => 'home_id',
	) );

	/* basic section */
	/*theme logo*/
	$wp_customize->add_setting( 'bizzbee_logo' ,array(
		'sanitize_callback' => 'esc_attr',
		)
	 );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'bizzbee_logo', array(
		'label'    => __( 'Logo (Recommended size 220 x 120)', 'bizzbee' ),
		'section'  => 'bizzbee_basic_section',
		'settings' => 'bizzbee_logo',
	) ) );
	
	// favicon
	$wp_customize->add_setting( 'bizzbee_favicon',array(
		'sanitize_callback' => 'esc_attr',
		)
	 );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'bizzbee_favicon', array(
    'label'    => __( 'Favicon (Recommended size 32 x 32)', 'bizzbee' ),
    'section'  => 'bizzbee_basic_section',
    'settings' => 'bizzbee_favicon',
	) ) );
	

	// blog title
	$wp_customize->add_setting( 'bizzbee_blogtitle', array(
            'default'        => ' ',
            'sanitize_callback' => 'bizzbee_sanitize_text',
        ) );
   $wp_customize->add_control( 'bizzbee_blogtitle', array(
		'label'   => __('Blog Title','bizzbee'),
		'section' => 'bizzbee_basic_section',
		'type'    => 'text',
        ) );

	// copyright
	$wp_customize->add_setting( 'copyright_url_setting', array(
		'default'        => '',
		'sanitize_callback' => 'bizzbee_sanitize_html',
	) );
	$wp_customize->add_control( 'copyright_url_setting', array(
		'label'   => __('Copyright text','bizzbee'),
		'section' => 'bizzbee_basic_section',
		'type'    => 'text'
	) );



	// home page	
	$wp_customize->add_setting( 'bizzbee_metaslider', array(
            'default'        => '',
            'sanitize_callback' => 'bizzbee_sanitize_number_absint',
        ) );
    $wp_customize->add_control( 'bizzbee_metaslider', array(
			'label'   => __('Meta Slider Id','bizzbee'),
            'section' => 'bizzbee_silder_section',
            'type'    => 'number',
           
        ) );  
       
     // Welcome Section  
     $wp_customize->add_setting( 'bizzbee_Welcome_title', array(
		'default'        => '',
		'sanitize_callback' => 'bizzbee_sanitize_text',
	) );
    $wp_customize->add_control( 'bizzbee_Welcome_title', array(
		'label'   => __('Welcome Title','bizzbee'),
		'section' => 'bizzbee_welcome_section',
		'type'    => 'text',
    ) );
    $wp_customize->add_setting( 'bizzbee_Welcome_info', array(
		'default'        => '',
		'sanitize_callback' => 'esc_textarea',
	) );
    $wp_customize->add_control( 'bizzbee_Welcome_info', array(
		'label'   => __('Welcome Info','bizzbee'),
        'section' => 'bizzbee_welcome_section',
        'type'    => 'textarea',
   ) );
   $wp_customize->add_setting( 'bizzbee_Welcome_righttitle', array(
		'default'        => '',
		'sanitize_callback' => 'bizzbee_sanitize_text',
	) );
    $wp_customize->add_control( 'bizzbee_Welcome_righttitle', array(
		'label'   => __('Welcome Right Title','bizzbee'),
		'section' => 'bizzbee_welcome_section',
		'type'    => 'text',
    ) );
    $wp_customize->add_setting( 'bizzbee_Welcome_rightinfo', array(
		'default'        => '',
		'sanitize_callback' => 'esc_textarea',
	) );
    $wp_customize->add_control( 'bizzbee_Welcome_rightinfo', array(
		'label'   => __('Welcome Right Info','bizzbee'),
        'section' => 'bizzbee_welcome_section',
        'type'    => 'textarea',
   ) );     
   $wp_customize->add_setting( 'bizzbee_Welcome_image_bg',array(
		'sanitize_callback' => 'bizzbee_sanitize_text',
		)
	);
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'bizzbee_Welcome_image_bg', array(
			'label'    => __( 'Center Image (Recommended size 570 x 350)', 'bizzbee' ),
			'section'  => 'bizzbee_welcome_section',
			'settings' => 'bizzbee_Welcome_image_bg',
		) 
	) ); 
   
     $wp_customize->add_setting( 'bizzbee_Welcome_lefttitle', array(
		'default'        => '',
		'sanitize_callback' => 'bizzbee_sanitize_text',
	) );
    $wp_customize->add_control( 'bizzbee_Welcome_lefttitle', array(
		'label'   => __('Welcome Left Title','bizzbee'),
		'section' => 'bizzbee_welcome_section',
		'type'    => 'text',
    ) );
    $wp_customize->add_setting( 'bizzbee_Welcome_leftinfo', array(
		'default'        => '',
		'sanitize_callback' => 'esc_textarea',
	) );
    $wp_customize->add_control( 'bizzbee_Welcome_leftinfo', array(
		'label'   => __('Welcome Left Info','bizzbee'),
        'section' => 'bizzbee_welcome_section',
        'type'    => 'textarea',
   ) );     
        
     //Our path to Perfection
	$wp_customize->add_setting( 'bizzbee_perfectiontitle', array(
		'default'        => '',
		'sanitize_callback' => 'bizzbee_sanitize_text',
	) );
    $wp_customize->add_control( 'bizzbee_perfectiontitle', array(
		'label'   => __('Perfection Title','bizzbee'),
		'section' => 'bizzbee_perfection_section',
		'type'    => 'text',
    ) );
	
	 $wp_customize->add_setting( 'bizzbee_perfectioninfo', array(
		'default'        => '',
		'sanitize_callback' => 'esc_textarea',
	) );
    $wp_customize->add_control( 'bizzbee_perfectioninfo', array(
		'label'   => __('Perfection Info','bizzbee'),
        'section' => 'bizzbee_perfection_section',
        'type'    => 'textarea',
   ) );
   $wp_customize->add_setting( 'bizzbee_perfection_buttontitle', array(
		'default'        => '',
		'sanitize_callback' => 'bizzbee_sanitize_text',
	) );
    $wp_customize->add_control( 'bizzbee_perfection_buttontitle', array(
		'label'   => __('Button Title','bizzbee'),
		'section' => 'bizzbee_perfection_section',
		'type'    => 'text',
    ) );
    
   $wp_customize->add_setting( 'bizzbee_perfection_buttonlink', array(
		'default'        => '',
		'sanitize_callback' => 'bizzbee_sanitize_url',
	) );
    $wp_customize->add_control( 'bizzbee_perfection_buttonlink', array(
		'label'   => __('Button Link','bizzbee'),
		'section' => 'bizzbee_perfection_section',
		'type'    => 'text',
    ) );
   
	$wp_customize->add_setting( 'bizzbee_breadcrumbs_image_bg',array(
		'sanitize_callback' => 'esc_attr',
		)
	);
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'bizzbee_breadcrumbs_image_bg', array(
			'label'    => __( 'Background Image (Recommended size 1280 x 853)', 'bizzbee' ),
			'section'  => 'bizzbee_perfection_section',
			'settings' => 'bizzbee_breadcrumbs_image_bg',
		) 
	) );   
        
  
   // What You Get
   $wp_customize->add_setting( 'bizzbee_get_title', array(
		'default'        => '',
		'sanitize_callback' => 'bizzbee_sanitize_text',
	) );
    $wp_customize->add_control( 'bizzbee_get_title', array(
		'label'   => __('Title','bizzbee'),
		'section' => 'bizzbee_whatyouget_section',
		'type'    => 'text',
    ) );
    
    $wp_customize->add_setting( 'bizzbee_get_info', array(
		'default'        => '',
		'sanitize_callback' => 'esc_textarea',
	) );
    $wp_customize->add_control( 'bizzbee_get_info', array(
		'label'   => __('What you Get Info','bizzbee'),
        'section' => 'bizzbee_whatyouget_section',
        'type'    => 'textarea',
	) );
	$wp_customize->add_setting( 'bizzbee_youget_image_bg',array(
		'sanitize_callback' => 'esc_attr',
		)
	);
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'bizzbee_youget_image_bg', array(
			'label'    => __( 'Right Side Image (Recommended size 570 x 350)', 'bizzbee' ),
			'section'  => 'bizzbee_whatyouget_section',
			'settings' => 'bizzbee_youget_image_bg',
		) 
	) ); 
    $wp_customize->add_setting( 'bizzbee_get_left_text', array(
		'default'        => '',
		'sanitize_callback' => 'esc_textarea',
	) );
    $wp_customize->add_control( 'bizzbee_get_left_text', array(
		'label'   => __('Left Side Text','bizzbee'),
        'section' => 'bizzbee_whatyouget_section',
        'type'    => 'textarea',
	) );
       
       
     //Blog Section
	$wp_customize->add_setting( 'bizzbee_blog_title', array(
		'default'        => '',
		'sanitize_callback' => 'bizzbee_sanitize_text',
	) );
    
    $wp_customize->add_control( 'bizzbee_blog_title', array(
		'label'   => __('Blog Title','bizzbee'),
        'section' => 'bizzbee_blog_section',
        'type'    => 'text'
    ) );
    
    $wp_customize->add_setting( 'bizzbee_breadcrumbs_blog_image_bg',array(
		'sanitize_callback' => 'esc_attr',
		)
	);
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'bizzbee_breadcrumbs_blog_image_bg', array(
			'label'    => __( 'Background Blog Image (Recommended size 1280 x 853)', 'bizzbee' ),
			'section'  => 'bizzbee_blog_section',
			'settings' => 'bizzbee_breadcrumbs_blog_image_bg',
		) 
	) ); 
        
	$bizzbee_args = array(
	'posts_per_page'=> -1,
	'meta_query' => array(
						array(
						'key' => '_thumbnail_id',
						'compare' => 'EXISTS'
							),
						)
					);  
	$bizzbee_post = new WP_Query( $bizzbee_args );
	$bizzbee_cat_id=array();
	while($bizzbee_post->have_posts()){
	$bizzbee_post->the_post();
	$bizzbee_post_categories = wp_get_post_categories( get_the_id());
	foreach($bizzbee_post_categories as $bizzbee_post_category)
		$bizzbee_cat_id[]=$bizzbee_post_category;
	}
	
	$bizzbee_cat_id=array_unique($bizzbee_cat_id);
	$bizzbee_args = array(
	'orderby' => 'name',
	'parent' => 0,
	'include'=>$bizzbee_cat_id,
	
	);
	$bizzbee_cats=array();$i = 0;
	$bizzbee_categories = get_categories($bizzbee_args); 
	  foreach ($bizzbee_categories as $bizzbee_category) {
		  if($i==0){
			$bizzbee_default = $bizzbee_category->term_id;
			$i++;
		}
		$bizzbee_cats[$bizzbee_category->term_id] =  $bizzbee_category->cat_name;
	  }        
      
	 $wp_customize->add_setting( 'bizzbee_blogcategory', array(
		'default'        => $bizzbee_default,
		'sanitize_callback' => 'esc_attr',
				
	) );
    
    $wp_customize->add_control( 'bizzbee_blogcategory', array(
			'label'   => __('Select Category','bizzbee'),
            'section' => 'bizzbee_blog_section',
            'type'    => 'select',
            'choices' => $bizzbee_cats,
        ) );
                
	$wp_customize->add_setting( 'bizzbee_metaslider', array(
            'default'        => '',
            'sanitize_callback' => 'bizzbee_sanitize_number_absint',
        ) );
    
    $wp_customize->add_control( 'bizzbee_metaslider', array(
			'label'   => __('Meta Slider Id','bizzbee'),
            'section' => 'bizzbee_silder_section',
            'type'    => 'number',
           
        ) );           

}
add_action( 'customize_register', 'bizzbee_theme_customizer' );

function bizzbee_sanitize_url( $bizzbee_url ) {
	return esc_url_raw( $bizzbee_url );
}
function bizzbee_sanitize_html( $bizzbee_html ) {
	return wp_filter_post_kses( $bizzbee_html );
}
function bizzbee_sanitize_number_absint( $bizzbee_number ) {
	// Ensure $number is an absolute integer (whole number, zero or greater).
	$number = absint( $bizzbee_number );
	// If the input is an absolute integer, return it; otherwise, return the default
	return ( $bizzbee_number ? $bizzbee_number : '' );
}
function bizzbee_sanitize_text( $bizzbee_input ) {
    return wp_kses_post( force_balance_tags( $bizzbee_input ) );
}

?>
