<?php
/*
 * thumbnail list
 */

function bizzbee_thumbnail_image($content) {

  if (has_post_thumbnail())
    return the_post_thumbnail('thumbnail');
}

/*
 * bizzbee Main Sidebar
 */

function bizzbee_widgets_init() {

  register_sidebar(array(
      'name' => __('Main Sidebar', 'bizzbee'),
      'id' => 'sidebar-1',
      'description' => __('Main sidebar that appears on the right.', 'bizzbee'),
      'before_widget' => '<div class="sidebar-widget %2$s" id="%1$s" >',
      'after_widget' => '</div>',
      'before_title' => '<h4 class="widget-title">',
      'after_title' => '</h4>',
  ));

  register_sidebar(array(
      'name' => __('Footer area one', 'bizzbee'),
      'id' => 'footer-1',
      'description' => __('Footer area one that appears on the footer.', 'bizzbee'),
      'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h4 class="widget-title">',
      'after_title' => '</h4>',
  ));

  register_sidebar(array(
      'name' => __('Footer area two', 'bizzbee'),
      'id' => 'footer-2',
      'description' => __('Footer area two that appears on the footer.', 'bizzbee'),
      'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h4 class="widget-title">',
      'after_title' => '</h4>',
  ));

  register_sidebar(array(
      'name' => __('Footer area three', 'bizzbee'),
      'id' => 'footer-3',
      'description' => __('Footer area three that appears on the footer.', 'bizzbee'),
      'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h4 class="widget-title">',
      'after_title' => '</h4>',
  ));

  register_sidebar(array(
      'name' => __('Footer area four', 'bizzbee'),
      'id' => 'footer-4',
      'description' => __('Footer area four that appears on the footer.', 'bizzbee'),
      'before_widget' => '<div id="%1$s" class="footer-widget %2$s">',
      'after_widget' => '</div>',
      'before_title' => '<h4 class="widget-title">',
      'after_title' => '</h4>',
  ));
}

add_action('widgets_init', 'bizzbee_widgets_init');

/*
 * bizzbee Set up post entry meta.
 *
 * Meta information for current post: categories, tags, permalink, author, and date.
 */

function bizzbee_entry_meta() {
  $bizzbee_categories_list = get_the_category_list(', ', ' ');
  $bizzbee_tag_list = get_the_tag_list('', ',');
  $bizzbee_author = get_the_author();
  $bizzbee_author_url = esc_url(get_author_posts_url(get_the_author_meta('ID')));
  $bizzbee_comments = wp_count_comments(get_the_ID());
  $bizzbee_date = sprintf('<time datetime="%1$s">%2$s</time>', sanitize_text_field(get_the_date('c')), esc_html(get_the_date())
  );
  ?>	
<div class="post-meta">
  <ul>
    <li><?php echo $bizzbee_date; ?></li>
    <li><a href="<?php echo $bizzbee_author_url; ?>" rel="tag"><?php echo _e('by:','bizzbee').$bizzbee_author; ?></a></li>		
    <?php if(!empty($bizzbee_tag_list)) { ?>				
    <li class="tag-list"><?php echo _e('Tags : ','bizzbee'). $bizzbee_tag_list; ?></li>
    <?php } ?>
    <li class="category-list"><?php echo _e('Posted in : ','bizzbee').  $bizzbee_categories_list; ?></li>
    <li><?php comments_number(__('No Comments', 'bizzbee'), __('1 Comment', 'bizzbee'), __('% Comments', 'bizzbee')); ?></li>
  </ul>
</div>                                        
  <?php
}

/*
 * pagination
 * */

function bizzbee_pagination() {
	if(is_single()){
		the_post_navigation( array(
			'prev_text' => '<div class="bizzbee_previous_pagination alignleft">%title</div>',
			'next_text' => '<div class="bizzbee_next_pagination alignright">%title</div>',
		) );
	}else{
		the_posts_pagination(array(
		  'prev_text' => '<i class="fa fa-angle-double-left"></i>',
		  'next_text' => '<i class="fa fa-angle-double-right"></i>',
		  'before_page_number' => '<span class="meta-nav screen-reader-text"></span>',
		));
	}
}

/*
 * Comments placeholder function
 * 
 * */
add_filter('comment_form_default_fields', 'bizzbee_comment_placeholders');

function bizzbee_comment_placeholders($fields) {
  $fields['author'] = str_replace(
          '<input', '<input placeholder="'
          . _x(
                  'Name *', 'comment form placeholder', 'bizzbee'
          )
          . '" required', $fields['author']
  );
  $fields['email'] = str_replace(
          '<input', '<input placeholder="'
          . _x(
                  'E-mail *', 'comment form placeholder', 'bizzbee'
          )
          . '" required', $fields['email']
  );
    $fields['url'] = str_replace(
          '<input', '<input placeholder="'
          . _x(
                  'Website *', 'comment form placeholder', 'bizzbee'
          )
          . '" required', $fields['url']
  );
  return $fields;
}

add_filter('comment_form_defaults', 'bizzbee_textarea_insert');

function bizzbee_textarea_insert($fields) {
  $fields['comment_field'] = str_replace(
          '<textarea', '<textarea  placeholder="'
          . _x(
                  'Comment', 'comment form placeholder', 'bizzbee'
          )
          . '" ', $fields['comment_field']
  );
  return $fields;
}
?>