<?php
/*
 * bizzbee Breadcrumbs
*/
function bizzbee_custom_breadcrumbs() {

  $bizzbee_showonhome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
  $bizzbee_delimiter = '/'; // bizzbee_delimiter between crumbs
  $bizzbee_home = __('Home','bizzbee'); // text for the 'Home' link
  $bizzbee_showcurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
  $bizzbee_before = ' '; // tag before the current crumb
  $bizzbee_after = ' '; // tag after the current crumb

  global $post;
  $bizzbee_homelink = esc_url(home_url('/'));

  if (is_home() || is_front_page()) {

    if ($bizzbee_showonhome == 1) echo '<li id="breadcrumbs"><a href="' . $bizzbee_homelink . '">' . $bizzbee_home . '</a></li>';
    
  }  else {

    echo '<li id="breadcrumbs_home"><a href="' . $bizzbee_homelink . '">' . $bizzbee_home . '</a> </li><li id="breadcrumbs">';
    
   if ( is_category() ) {
      $bizzbee_thisCat = get_category(get_query_var('cat'), false);
      if ($bizzbee_thisCat->parent != 0) echo get_category_parents($bizzbee_thisCat->parent, TRUE, ' ' . $bizzbee_delimiter . ' ');      
		echo $bizzbee_before; _e('category','bizzbee'); echo ' "'.single_cat_title('', false) . '"' . $bizzbee_after;
    } 
    elseif ( is_search() ) {
      echo $bizzbee_before; _e('Search Results For','bizzbee'); echo ' "'. get_search_query() . '"' . $bizzbee_after;

    } elseif ( is_day() ) {
      echo '<a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . get_the_time('Y') . '</a> ' . $bizzbee_delimiter . ' ';
      echo '<a href="' . esc_url(get_month_link(get_the_time('Y'),get_the_time('m'))) . '">' . get_the_time('F') . '</a> ' . $bizzbee_delimiter . ' ';
      echo $bizzbee_before . get_the_time('d') . $bizzbee_after;

    } elseif ( is_month() ) {
      echo '<a href="' . esc_url(get_year_link(get_the_time('Y'))) . '">' . get_the_time('Y') . '</a> ' . $bizzbee_delimiter . ' ';
      echo $bizzbee_before . get_the_time('F') . $bizzbee_after;

    } elseif ( is_year() ) {
      echo $bizzbee_before . get_the_time('Y') . $bizzbee_after;

    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() != 'post' ) {
        $bizzbee_post_type = get_post_type_object(get_post_type());
        $bizzbee_slug = $bizzbee_post_type->rewrite;
        echo '<a href="' . $bizzbee_homelink . '/' . $bizzbee_slug['slug'] . '/">' . $bizzbee_post_type->labels->singular_name . '</a>';
        if ($bizzbee_showcurrent == 1) echo ' ' . $bizzbee_delimiter . ' ' . $bizzbee_before . get_the_title() . $bizzbee_after;
      } else {
        $bizzbee_cat = get_the_category(); $bizzbee_cat = $bizzbee_cat[0];
        $bizzbee_cats = get_category_parents($bizzbee_cat, TRUE, ' ' . $bizzbee_delimiter . ' ');
        if ($bizzbee_showcurrent == 0) $bizzbee_cats = preg_replace("#^(.+)\s$bizzbee_delimiter\s$#", "$1", $bizzbee_cats);
        echo $bizzbee_cats;
        if ($bizzbee_showcurrent == 1) echo $bizzbee_before . get_the_title() . $bizzbee_after;
      }

    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $bizzbee_post_type = get_post_type_object(get_post_type());
      echo $bizzbee_before . $bizzbee_post_type->labels->singular_name . $bizzbee_after;

    } elseif ( is_attachment() ) {
      $bizzbee_parent = get_post($post->post_parent);
      $bizzbee_cat = get_the_category($bizzbee_parent->ID); $bizzbee_cat = $bizzbee_cat[0];
      echo get_category_parents($bizzbee_cat, TRUE, ' ' . $bizzbee_delimiter . ' ');
      echo '<a href="' . esc_url(get_permalink($bizzbee_parent)) . '">' . $bizzbee_parent->post_title . '</a>';
      if ($bizzbee_showcurrent == 1) echo ' ' . $bizzbee_delimiter . ' ' . $bizzbee_before . get_the_title() . $bizzbee_after;

    } elseif ( is_page() && !$post->post_parent ) {
      if ($bizzbee_showcurrent == 1) echo $bizzbee_before . get_the_title() . $bizzbee_after;

    } elseif ( is_page() && $post->post_parent ) {
      $bizzbee_parent_id  = $post->post_parent;
      $bizzbee_breadcrumbs = array();
      while ($bizzbee_parent_id) {
        $bizzbee_page = get_page($bizzbee_parent_id);
        $bizzbee_breadcrumbs[] = '<a href="' . esc_url(get_permalink($bizzbee_page->ID)) . '">' . get_the_title($bizzbee_page->ID) . '</a>';
        $bizzbee_parent_id  = $bizzbee_page->post_parent;
      }
      $bizzbee_breadcrumbs = array_reverse($bizzbee_breadcrumbs);
      for ($bizzbee_i = 0; $bizzbee_i < count($bizzbee_breadcrumbs); $bizzbee_i++) {
        echo $bizzbee_breadcrumbs[$bizzbee_i];
        if ($bizzbee_i != count($bizzbee_breadcrumbs)-1) echo ' ' . $bizzbee_delimiter . ' ';
      }
      if ($bizzbee_showcurrent == 1) echo ' ' . $bizzbee_delimiter . ' ' . $bizzbee_before . get_the_title() . $bizzbee_after;

    } elseif ( is_tag() ) {
      echo $bizzbee_before; _e('Posts tagged','bizzbee'); echo ' "'.  single_tag_title('', false) . '"' . $bizzbee_after;

    } elseif ( is_author() ) {
       global $author;
      $bizzbee_userdata = get_userdata($author);
      echo $bizzbee_before; _e('Articles posted by ','bizzbee'); echo $bizzbee_userdata->display_name . $bizzbee_after;

    } elseif ( is_404() ) {
      echo $bizzbee_before; _e('Error 404','bizzbee'); echo $bizzbee_after;
    }
    
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Page','bizzbee') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
    echo '</li>';

  }
} // end bizzbee_custom_breadcrumbs()
