<?php
function bizzbee_options_init(){
 register_setting( 'bizzbee_options', 'bizzbee_theme_options','bizzbee_options_validate');
} 
add_action( 'admin_init', 'bizzbee_options_init' );
function bizzbee_framework_load_scripts(){
	wp_enqueue_media();
	wp_enqueue_style( 'bizzbee_framework', get_template_directory_uri(). '/theme-options/css/theme-option_framework.css' ,false, '1.0.0');	
	wp_localize_script('options-custom', 'admin_url', admin_url('admin-ajax.php'));
}
add_action( 'admin_enqueue_scripts', 'bizzbee_framework_load_scripts' );
function bizzbee_framework_menu_settings() {
	$bizzbee_menu = array(
				'page_title' => __( 'Bizzbee Options', 'bizzbee_framework'),
				'menu_title' => __('Bizzbee Pro Features', 'bizzbee_framework'),
				'capability' => 'edit_theme_options',
				'menu_slug' => 'bizzbee_framework',
				'callback' => 'bizzbee_framework_page'
				);
	return apply_filters( 'bizzbee_framework_menu', $bizzbee_menu );
}
add_action( 'admin_menu', 'bizzbee_options_add_page' ); 
function bizzbee_options_add_page() {
	$bizzbee_menu = bizzbee_framework_menu_settings();
   	add_theme_page($bizzbee_menu['page_title'],$bizzbee_menu['menu_title'],$bizzbee_menu['capability'],$bizzbee_menu['menu_slug'],$bizzbee_menu['callback']);
} 
function bizzbee_framework_page(){ 
		global $select_options; 
		if ( ! isset( $_REQUEST['settings-updated'] ) ) 
		$_REQUEST['settings-updated'] = false;		

?>
<div class="theme-option-themes">
	<form method="post" action="options.php" id="form-option" class="theme_option_ft">
  <div class="theme-option-header">
    <div class="logo">
       <?php
		$bizzbee_image=get_template_directory_uri().'/theme-options/images/logo.png';
		echo "<a href='http://fruitthemes.com' target='_blank'><img src='".$bizzbee_image."' alt='fruitthemes' /></a>";
		?>
    </div>
  </div>
  <div class="theme-option-details">
    <div class="theme-option-options">
      <div class="right-box">
        <div class="nav-tab-wrapper">
          <ul>
            <li><a id="options-group-1-tab" class="nav-tab basicsettings-tab" title="<?php _e('PRO Theme Features','bizzbee'); ?>" href="#options-group-1"><?php _e('PRO Theme Features','bizzbee'); ?></a></li>
          </ul>  
        </div>
      </div>
      <div class="right-box-bg"></div>
      <div class="postbox left-box"> 
        <!--======================== F I N A L - - T H E M E - - O P T I O N ===================-->
          <?php settings_fields( 'bizzbee_options' );  
		$bizzbee_options = get_option( 'bizzbee_theme_options' );
		 ?>
          <div id="options-group-1" class="group theme-option-inner-tabs"> 
				<div class="bizzbee-pro-header">
              <h2 class="bizzbee-pro-logo">bizzbee PRO</h2>
              <a href="http://fruitthemes.com/wordpress-themes/bizzbee" target="_blank">
					<img src="<?php echo get_template_directory_uri(); ?>/theme-options/images/bizzbee-buy-now.png" class="bizzbee-pro-buynow" /></a>  
              </div>
          	<img src="<?php echo get_template_directory_uri(); ?>/theme-options/images/bizzbee_pro.png" class="bizzbee-pro-image" />
		  </div>
      </div>
     </div>
	</div>
   </form>    
</div>
<?php } ?>